$(document).on('submit', '.add-to-cart-form', function (event) {
  event.preventDefault()
  var $form = $(this)
  $.ajax({
    url: $form.data('ajax-url'),
    data: $form.serialize(),
    dataType: 'html',
    method: 'post',
    success: function (result) {
      $form.find('.ajax-content').html(result)
      setTimeout(function() {
        $form.find('.ajax-content').find('div').fadeOut('slow', function() {
          $(this).remove();
        });
      }, 4000);
    }
  })
})