<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$run = function ($_EXTKEY) {
    /*
     * Configure plugin cart.
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Riconet.'.$_EXTKEY,
        'cart',
        ['Cart' => 'index, addItem, addItemAjax, removeItem, edit, clear', 'Widget' => 'index'],
        ['Cart' => 'index, addItem, addItemAjax, removeItem, edit, clear', 'Widget' => 'index']
    );

    /*
     * Configure plugin checkout.
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Riconet.'.$_EXTKEY,
        'checkout',
        ['Checkout' => 'index, order'],
        ['Checkout' => 'index, order']
    );

    /*
     * Configure plugin history.
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Riconet.'.$_EXTKEY,
        'history',
        ['History' => 'index']
    );
};

$run($_EXTKEY);
unset($run);
