<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Helper;

use Riconet\RicoOrder\Domain\Model\CartItem;
use Riconet\RicoOrder\Domain\Model\Order;
use Riconet\RicoOrder\Domain\Model\Position;
use Riconet\RicoOrder\Domain\Model\Section;
use Riconet\RicoOrder\Utility\TotalComputeUtility;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Mvc\Request;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;

/**
 * Class OrderHelper.
 */
class OrderHelper implements SingletonInterface
{
    /**
     * The orderRepository.
     *
     * @var \Riconet\RicoOrder\Domain\Repository\OrderRepository
     * @inject
     */
    protected $orderRepository = null;

    /**
     * The $ignalSlotDispatcher.
     *
     * @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher
     * @inject
     */
    protected $signalSlotDispatcher = null;

    /**
     * the ObjectManager instance.
     *
     * @var null|ObjectManager
     */
    protected $objectManager = null;

    /**
     * injects the ObjectManager.
     *
     * @param ObjectManager $objectManager the ObjectManager instance
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * sections.
     *
     * @var array|Section
     */
    protected $sections = [];

    /**
     * @var int
     */
    protected $pid = 0;

    /**
     * OrderHelper constructor.
     */
    public function __construct()
    {
        /** @var $objectManager ObjectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var $configurationManager ConfigurationManagerInterface */
        $configurationManager = $objectManager->get(ConfigurationManagerInterface::class);
        $typoScript = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        );
        $this->pid = isset($typoScript['persistence']['storagePid']) ? (int) $typoScript['persistence']['storagePid'] : 0;
    }

    /**
     * Create a new order.
     *
     * @param Request        $request
     * @param array|CartItem $items
     * @param mixed          $user
     * @param string         $customerNumber
     * @param array          $settings
     *
     * @throws IllegalObjectTypeException
     */
    public function createNewOrder(Request $request, array $items, $user, $customerNumber, array $settings)
    {
        // The used vat to calculate the totals.
        $configurations = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['rico_order']);
        $vat = round($configurations['vat'], 2);
        // Signal beforeOrderCreated
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            'beforeOrderCreated',
            [$this, $items, $user, $customerNumber, $request, $settings]
        );
        /** @var Order $order */
        $order = $this->objectManager->get(Order::class);
        $order = $this->attachSectionsToOrder($order, $items);
        $order->setCreationDateTime(new \DateTime());
        $order->setCustomerNumber($customerNumber);
        if ($user instanceof FrontendUser) {
            $order->setFrontendUser($user);
        }
        $order->setTotalWithoutVat(TotalComputeUtility::computeTotalWithoutVat($items));
        $order->setTotalWithVat(TotalComputeUtility::computeTotalWithVat($items, $vat));
        $order->setTotalVat(TotalComputeUtility::computeTotalVat($items, $vat));
        $order->setPid($this->pid);
        // Signal beforeOrderPersisted.
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            'beforeOrderPersisted',
            [$this, $order, $request, $settings]
        );
        $this->orderRepository->add($order);
        // Signal afterOrderPersisted.
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            'afterOrderPersisted',
            [$this, $order, $request, $settings]
        );
    }

    /**
     * Create all sections and store them in the class variable sections.
     *
     * @param array $items
     */
    protected function createSections(array $items)
    {
        /** @var $item CartItem */
        foreach ($items as $item) {
            $section = $this->createSection($item);
            if (!is_null($section)) {
                $this->sections[] = $section;
            }
        }
    }

    /**
     * Create and return a new section.
     *
     * @param CartItem $item
     *
     * @return Section|null
     */
    private function createSection(CartItem $item)
    {
        if ($this->sectionsContainTitle($item->getSection())) {
            return null;
        }

        $section = new Section();
        $section->setTitle($item->getSection());
        $section->setPid($this->pid);

        return $section;
    }

    /**
     * Checks if the sections array contains a section with a given title.
     *
     * @param string $title
     *
     * @return bool
     */
    private function sectionsContainTitle($title)
    {
        $title = empty($title) ? 'DEFAULT' : $title;
        /** @var $section Section */
        foreach ($this->sections as $section) {
            if (trim($section->getTitle()) === trim($title)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Associates the items with the created sections.
     *
     * @param array $items
     */
    private function associateSectionsWithPositions(array $items)
    {
        $index = 0;
        /** @var $item CartItem */
        foreach ($items as $index => $item) {
            /** @var Position $newPosition */
            $newPosition = $this->objectManager->get(Position::class);
            $newPosition->setTitle($item->getTitle());
            $newPosition->setQuantity($item->getQuantity());
            $newPosition->setUnitPrice($item->getPrice());
            $newPosition->setPrice(
                round($item->getPrice() * $item->getQuantity(), 2,PHP_ROUND_HALF_UP)
            );
            $newPosition->setArticleNumber($item->getArticleNumber());
            $newPosition->setRecordTable($item->getRecordTable());
            $newPosition->setRecordUid($item->getRecordUid());
            $newPosition->setPid($this->pid);
            // Signal beforeOrderPersisted.
            $this->signalSlotDispatcher->dispatch(
                __CLASS__,
                'beforeAttachNewPosition',
                [&$newPosition, $item, $index]
            );
            /** @var $section Section */
            foreach ($this->sections as &$section) {
                if (trim($section->getTitle()) === trim($item->getSection())) {
                    $section->getPositions()->attach($newPosition);
                }
            }
            $index++;
        }
        // Signal afterAttachPositions.
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            'afterAttachPositions',
            [&$this->sections, $items]
        );
    }

    /**
     * Attach sections on he given order.
     *
     * @param Order $order
     * @param array $items
     *
     * @return Order
     */
    private function attachSectionsToOrder(Order $order, array $items)
    {
        $this->createSections($items);
        $this->associateSectionsWithPositions($items);
        /** @var $section Section */
        foreach ($this->sections as $section) {
            $order->getSections()->attach($section);
        }

        return $order;
    }
}
