<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Utility;

use Riconet\RicoOrder\Domain\Model\CartItem;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidActionNameException;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Class CartUtility.
 */
class CartUtility
{
    /**
     * Converts an domain object in to a cart item.
     *
     * @param AbstractDomainObject $domainObject
     * @param int                  $quantity
     * @param string               $titleField
     * @param int                  $sectionUid
     *
     * @return object|CartItem
     */
    public static function convertDomainObjectToCartItem(
        AbstractDomainObject $domainObject,
        $quantity,
        $titleField = 'title',
        $sectionUid = null
    ) {
        $recordTable = DomainObjectUtility::GetTableNameByDomainObject($domainObject);
        $recordUid = $domainObject->getUid();
        $title = $domainObject->_getProperty($titleField);
        $class = get_class($domainObject);
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $cartItem = $objectManager->get(
            CartItem::class, $title, $recordTable, $recordUid, $quantity, $class, $sectionUid
        );

        return $cartItem;
    }

    /**
     * Adds a new item to the cart or increase the quantity of an already contained item.
     *
     * @param array    $items
     * @param CartItem $newItem
     *
     * @return array
     */
    public static function addItem(array $items, CartItem $newItem)
    {
        $cartAlreadyContainsItem = false;
        /** @var $item CartItem */
        foreach ($items as &$item) {
            if ($item->getRecordUid() === $newItem->getRecordUid() && $item->getRecordTable() === $newItem->getRecordTable()
                && $item->getSection() === $newItem->getSection()) {
                $item->setQuantity($item->getQuantity() + $newItem->getQuantity());
                $cartAlreadyContainsItem = true;
            }
        }
        if (!$cartAlreadyContainsItem) {
            $items[] = $newItem;
        }

        return array_values($items);
    }

    /**
     * Removes an item from the cart.
     *
     * @param array    $items
     * @param CartItem $itemToRemove
     *
     * @return array
     */
    public static function removeItem(array $items, CartItem $itemToRemove)
    {
        for ($i = 0; $i < count($items); ++$i) {
            if (!$items[$i] instanceof CartItem) {
                continue;
            }
            if ($items[$i]->getRecordUid() === $itemToRemove->getRecordUid()
                && $items[$i]->getRecordTable() === $itemToRemove->getRecordTable()
                && $items[$i]->getSection() === $itemToRemove->getSection()
            ) {
                unset($items[$i]);
            }
        }

        return array_values($items);
    }

    /**
     * Update an item quantity.
     *
     * @param array    $items
     * @param CartItem $itemToUpdate
     * @param int      $quantity
     *
     * @throws InvalidActionNameException
     *
     * @return array
     */
    public static function updateItemQuantity(array $items, CartItem $itemToUpdate, $quantity)
    {
        if (!($quantity > 0)) {
            throw new InvalidActionNameException('The quantity must be greater than 0');
        }
        for ($i = 0; $i < count($items); ++$i) {
            /** @var $item CartItem */
            $item = &$items[$i];
            if ($item->getRecordUid() === $itemToUpdate->getRecordUid() && $item->getRecordTable() === $itemToUpdate->getRecordTable()
                && $item->getSection() === $itemToUpdate->getSection()) {
                $item->setQuantity($quantity);
            }
        }

        return array_values($items);
    }

    /**
     * Gets the in typo script mapped price for the CartItem.
     *
     * @param array    $ts
     * @param CartItem $cartItem
     * @param string   $property
     *
     * @return float
     *
     * @throws \Exception
     */
    public static function getMappedProperty(array $ts, CartItem $cartItem, $property)
    {
        if (!isset($ts['mapping'])) {
            throw new \Exception('TypoScript error: plugin.tx_ricoorder.mapping.* not found!');
        }
        $uid = $cartItem->getRecordUid();
        $table = $cartItem->getRecordTable();
        $class = $cartItem->getClass();
        // Look for class mapping
        foreach ($ts['mapping'] as $class => $properties) {
            if ($class == $class) {
                $parentClass = get_parent_class($class);
                $baseClass = $parentClass != AbstractEntity::class ? $parentClass : $class;
                $maxLoops = 99; // Avoid an infinity loop.
                while ($parentClass != AbstractEntity::class || $maxLoops <= 0) {
                    $parentClass = get_parent_class($parentClass);
                    if ($parentClass != AbstractEntity::class) {
                        $baseClass = $parentClass;
                    }
                    --$maxLoops;
                }
                /** @var $objectManager ObjectManager */
                $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
                /** @var $persistenceManger PersistenceManager */
                $persistenceManger = $objectManager->get(PersistenceManager::class);
                $query = $persistenceManger->createQueryForType($baseClass);
                $query->getQuerySettings()->setRespectStoragePage(false);
                $query->matching($query->equals('uid', $uid));
                $record = $query->execute()->getFirst();
                if ($record instanceof $baseClass && property_exists($baseClass, $properties[$property])) {
                    return $record->_getProperty($properties[$property]);
                }
                throw new \Exception("Mapping failed! Could not map given class:'$class (Base class: '$baseClass')' and property:'".
                    $properties[$property]."' and uid:'$uid''");
            }
        }
        throw new \Exception("Could not find any mappings for record with table:'$table', class:'$class' and uid:'$uid''");
    }
}
