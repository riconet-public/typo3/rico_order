<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Utility;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Class FlashMessageUtility
 * @package Riconet\RicoOrder\Utility
 */
class FlashMessageUtility
{

    /**
     * Create a translated string for key "tx_ricoorder_cart_flashmessage.add_item".
     *
     * @param $title
     * @return null|string
     */
    public static function addItem($title)
    {
        return LocalizationUtility::translate(
            'tx_ricoorder_cart_flashmessage.add_item',
                'RicoOrder',
                [$title]
        );
    }

    /**
     * Create a translated string for key "tx_ricoorder_cart_flashmessage.remove_item".
     *
     * @param $title
     * @return null|string
     */
    public static function removeItem($title)
    {
        return LocalizationUtility::translate(
            'tx_ricoorder_cart_flashmessage.remove_item',
            'RicoOrder',
            [$title]
        );
    }

    /**
     * Create a translated string for key "tx_ricoorder_cart_flashmessage.update_item".
     *
     * @param $title
     * @return null|string
     */
    public static function updateItem($title)
    {
        return LocalizationUtility::translate(
            'tx_ricoorder_cart_flashmessage.update_item',
            'RicoOrder',
            [$title]
        );
    }

    /**
     * Create a translated string for key "tx_ricoorder_cart_flashmessage.update_items".
     *
     * @return null|string
     */
    public static function updateItems()
    {
        return LocalizationUtility::translate(
            'tx_ricoorder_cart_flashmessage.update_items',
            'RicoOrder'
        );
    }

    /**
     * Create a translated string for key "tx_ricoorder_checkout_flashmessage.created_order".
     *
     * @return null|string
     */
    public static function createdNewOrder()
    {
        return LocalizationUtility::translate(
            'tx_ricoorder_checkout_flashmessage.created_order',
            'RicoOrder'
        );
    }
}
