<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *      Created on: 06.09.17 09:34
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Utility;

use Riconet\RicoOrder\Domain\Model\CartItem;

/**
 * Class TotalComputeUtility.
 */
class TotalComputeUtility
{
    /**
     * Computes the total without vat of the given items.
     *
     * @param array $items
     *
     * @return float
     */
    public static function computeTotalWithoutVat(array $items)
    {
        $sum = 0.0;
        /** @var $item CartItem */
        foreach ($items as $item) {
            $multiplier = $item->getQuantity();
            $piecePrice = $item->getPrice();
            $sum += round(($piecePrice * $multiplier), 2,PHP_ROUND_HALF_UP);
        }

        return $sum;
    }

    /**
     * Computes the total with vat of the given items.
     *
     * @param array $items
     * @param float $vat
     *
     * @return float
     */
    public static function computeTotalWithVat(array $items, $vat)
    {
        $totalWithoutVat = round(self::computeTotalWithoutVat($items), 2);

        return $totalWithoutVat + (($totalWithoutVat / 100) * $vat);
    }

    /**
     * Computes the total vat of the given items.
     *
     * @param array $items
     * @param float $vat
     *
     * @return float
     */
    public static function computeTotalVat(array $items, $vat)
    {
        $totalWithoutVat = round(self::computeTotalWithoutVat($items), 2);
        $totalWithVat = round(self::computeTotalWithVat($items, $vat), 2);

        return $totalWithVat - $totalWithoutVat;
    }
}
