<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 WPU
 *
 *  Date: 22.05.2017
 *  Time: 11:10
 *
 *  All rights reserved
 *
 *  This project is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Utility;

use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;

/**
 * Class ControllerUtility
 * @package Riconet\RicoOrder\Utility
 */
class ControllerUtility
{

    /**
     * @param \TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext $controllerContext
     * @param $pid
     * @return string
     */
    public static function redirectToPid(ControllerContext $controllerContext, $pid)
    {
        $uriBuilder = $controllerContext->getUriBuilder();
        $uriBuilder->reset();
        $uriBuilder->setTargetPageUid($pid);
        $uri = $uriBuilder->build();
        return $uri;
    }

}