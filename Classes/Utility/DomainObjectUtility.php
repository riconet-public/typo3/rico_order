<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Utility;

use TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class DomainObjectUtility
 * @package Riconet\RicoOrder\Utility
 */
class DomainObjectUtility
{

    /**
     * Converts a domain object to an array.
     *
     * @param AbstractDomainObject $domainObject
     * @return array
     */
    public static function ConvertDomainObjectToArray(AbstractDomainObject $domainObject)
    {
        return (array) $domainObject;
    }

    /**
     * Generates a table name by a given domain object.
     *
     * @param AbstractDomainObject $domainObject
     * @return string The table name of the entity.
     */
    public static function GetTableNameByDomainObject(AbstractDomainObject $domainObject)
    {
        // Get the class name.
        $class = \get_class($domainObject);
        $parentClass = get_parent_class($class);
        $baseClass = $parentClass != AbstractEntity::class ? $parentClass : $class;
        $maxLoops = 99; // Avoid an infinity loop.
        while ($parentClass != AbstractEntity::class || $maxLoops <= 0) {
            $parentClass = get_parent_class($parentClass);
            if ($parentClass != AbstractEntity::class) {
                $baseClass = $parentClass;
            }
            --$maxLoops;
        }
        // Remove the vendor path.
        $classPaths = explode('\\', strtolower($baseClass));
        unset($classPaths[0]);
        return 'tx_'.implode('_', $classPaths);
    }

}
