<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\ViewHelpers;

use Riconet\RicoOrder\Domain\Model\Order;
use Riconet\RicoOrder\Domain\Model\Position;
use Riconet\RicoOrder\Domain\Model\Section;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class OrderSumCalculationViewHelper
 * @package Riconet\RicoOrder\ViewHelpers
 */
class OrderSumCalculationViewHelper extends AbstractViewHelper
{
    use \TYPO3\CMS\FluidStyledContent\ViewHelpers\Menu\MenuViewHelperTrait;

    /**
     * initializeArguments
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('order', Order::class, '', true);
        $this->registerArgument('as', 'string', '', true);
        $this->registerArgument('vat', 'double', '', false, 19.00);
    }

    /**
     * Renders the ViewHelper.
     *
     * @return string
     */
    public function render()
    {
        return $this->renderChildrenWithVariables(array(
            $this->arguments['as'] => [
                'netto' => round($this->computeNettoSum($this->arguments['order']), 2),
                'brutto' => round($this->computeBruttoSum($this->arguments['order'], $this->arguments['vat']),2),
                'difference' => round($this->computeBruttoNettoDifference($this->arguments['order'], $this->arguments['vat']), 2),
                'vat' => $this->arguments['vat']
            ]
        ));
    }

    /**
     * Computes the netto sum of the given items.
     *
     * @param Order $order
     * @return double
     */
    private function computeNettoSum(Order $order)
    {
        $sum = 0.0;
        /** @var $section Section */
        foreach($order->getSections() as $section) {
            /** @var $position Position */
            foreach($section->getPositions() as $position) {
                $multiplier = $position->getQuantity();
                $piecePrice = $position->getPrice();
                $sum += ($piecePrice * $multiplier);
            }
        }
        return $sum;
    }

    /**
     * Computes the brutto sum of the given items.
     *
     * @param Order $order
     * @param double $vat
     * @return double
     */
    private function computeBruttoSum(Order $order, $vat)
    {
        $nettoSum = round(self::computeNettoSum($order),2 );
        return $nettoSum + (($nettoSum / 100) * $vat);
    }

    /**
     * Computes the difference between the netto and the brutto sum.
     *
     * @param Order $order
     * @param double $vat
     * @return double
     */
    private function computeBruttoNettoDifference(Order $order, $vat)
    {
        $nettoSum = round(self::computeNettoSum($order), 2);
        $bruttoSum = round(self::computeBruttoSum($order, $vat), 2);
        return $bruttoSum - $nettoSum;
    }

}