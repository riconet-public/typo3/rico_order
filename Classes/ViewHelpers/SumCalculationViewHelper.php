<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\ViewHelpers;

use Riconet\RicoOrder\Domain\Model\CartItem;
use Riconet\RicoOrder\Utility\TotalComputeUtility;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class SumCalculationViewHelper.
 */
class SumCalculationViewHelper extends AbstractViewHelper
{
    use \TYPO3\CMS\FluidStyledContent\ViewHelpers\Menu\MenuViewHelperTrait;

    /**
     * initializeArguments.
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('items', 'array', '', true);
        $this->registerArgument('as', 'string', '', true);
    }

    /**
     * Renders the ViewHelper.
     *
     * @return string
     */
    public function render()
    {
        $configurations = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['rico_order']);
        $vat = round($configurations['vat'], 2);
        $items = $this->arguments['items'];
        $this->validateItemArray($items);

        return $this->renderChildrenWithVariables([
            $this->arguments['as'] => [
                'netto' => TotalComputeUtility::computeTotalWithoutVat($items),
                'brutto' => TotalComputeUtility::computeTotalWithVat($items, $vat),
                'difference' => TotalComputeUtility::computeTotalVat($items, $vat),
                'vat' => $vat,
            ],
        ]);
    }

    /**
     * Validates the item array.
     *
     * @param array|CartItem $items
     *
     * @throws Exception
     */
    protected function validateItemArray(array $items)
    {
        foreach ($items as $item) {
            if (!($item instanceof CartItem)) {
                throw new Exception(self::class.': Items array must only contain entries of type: '.CartItem::class);
            }
        }
    }
}
