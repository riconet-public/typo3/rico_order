<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Wolf Utz <utz@riconet.de>, riconet
 *      Created on: 20.04.18 14:25
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class RoundViewHelper.
 *
 * his ViewHelper implements the php round function in fluid.
 */
class RoundViewHelper extends AbstractViewHelper
{
    /**
     * Renders the view helper.
     *
     * Possible modes:
     * PHP_ROUND_HALF_UP
     * PHP_ROUND_HALF_DOWN
     * PHP_ROUND_HALF_EVEN
     * PHP_ROUND_HALF_ODD
     *
     * @see http://php.net/manual/de/function.round.php
     *
     * @param float  $val
     * @param int    $precision
     * @param string $mode
     *
     * @return float
     */
    public function render($val, $precision = 0, $mode = 'PHP_ROUND_HALF_UP')
    {
        return round($val, $precision, constant($mode));
    }
}
