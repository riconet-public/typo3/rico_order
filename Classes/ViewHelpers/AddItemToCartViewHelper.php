<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\ViewHelpers;

use TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class AddItemToProjectViewHelper.
 */
class AddItemToCartViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'a';

    /**
     * @param int                  $pageUid
     * @param AbstractDomainObject $domainObject
     * @param int                  $quantity
     * @param string               $section
     * @param int                  $returnPid
     * @param string               $titleField
     *
     * @return string
     */
    public function render(
        $pageUid,
        AbstractDomainObject $domainObject,
        $quantity = 1,
                            $section = 'Default',
        $returnPid = null,
        $titleField = 'title'
    ) {
        $uriBuilder = $this->controllerContext->getUriBuilder();
        $arguments = $this->getArguments($returnPid, $domainObject, $quantity, $titleField, $section);
        $uri = $uriBuilder
            ->reset()
            ->setTargetPageUid($pageUid)
            ->uriFor('addItem', $arguments, 'Cart', 'RicoOrder', 'Cart');
        $this->tag->addAttribute('href', $uri);
        $this->tag->setContent($this->renderChildren());
        $this->tag->forceClosingTag(true);

        return $this->tag->render();
    }

    /**
     * Gets the arguments.
     *
     * @param int                  $returnPid
     * @param AbstractDomainObject $domainObject
     * @param int                  $quantity
     * @param string               $titleField
     * @param string               $section
     *
     * @return array
     */
    private function getArguments($returnPid, AbstractDomainObject $domainObject, $quantity, $titleField, $section)
    {
        return [
            'domainObjectUid' => $domainObject->getUid(),
            'domainObjectClass' => get_class($domainObject),
            'quantity' => $quantity,
            'returnPid' => $returnPid,
            'titleField' => $titleField,
            'section' => $section,
        ];
    }
}
