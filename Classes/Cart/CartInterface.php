<?php
/*
 * This file is part of the "rico_order" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2019 Wolf Utz <utz@riconet.de>, riconet
 */

namespace Riconet\RicoOrder\Cart;

/**
 * Interface CartInterface.
 */
interface CartInterface
{
    /**
     * Initializes the cart.
     */
    public function initialize();

    /**
     * Gets all cart items as an array.
     *
     * @return array the items of the cart
     */
    public function getItems();

    /**
     * Sets the cart items.
     *
     * @param array $items the new items of the cart
     */
    public function setItems(array $items);

    /**
     * Removes an item from the cart.
     *
     * @param mixed $item the item to remove
     */
    public function removeItem($item);

    /**
     * Adds an item to the cart.
     *
     * @param mixed $item the item to add
     */
    public function addItem($item);

    /**
     * Removes all items from the cart.
     */
    public function destroy();
}
