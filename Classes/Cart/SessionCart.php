<?php
/*
 * This file is part of the "rico_order" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2019 Wolf Utz <utz@riconet.de>, riconet
 */

namespace Riconet\RicoOrder\Cart;

use Riconet\RicoOrder\Service\SessionService;
use TYPO3\CMS\Core\SingletonInterface;

/**
 * Class SessionCart.
 *
 * This implementation of the cart interface
 * uses PHP sessions, to store the cart of the user.
 */
class SessionCart implements CartInterface, SingletonInterface
{
    /**
     * @var string SESSION_PREFIX
     */
    const SESSION_PREFIX = 'rico_order';

    /**
     * @var string SESSION_KEY
     */
    const SESSION_KEY = 'cart';

    /**
     * the SessionService instance.
     *
     * @var null|SessionService
     */
    protected $sessionService;

    /**
     * injects the SessionService.
     *
     * @param SessionService $sessionService the SessionService instance
     */
    public function injectSessionService(SessionService $sessionService)
    {
        $this->sessionService = $sessionService;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize()
    {
        $this->sessionService->initSession(self::SESSION_PREFIX);
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        $items = $this->sessionService->getSessionData(self::SESSION_KEY);

        return is_array($items) ? $items : [];
    }

    /**
     * {@inheritdoc}
     */
    public function setItems(array $items)
    {
        $this->sessionService->setSessionData(self::SESSION_KEY, $items);
    }

    /**
     * {@inheritdoc}
     */
    public function removeItem($item)
    {
        $items = $this->getItems();
        if (0 == count($items)) {
            return;
        }
        for ($i = 0; $i < count($items); ++$i) {
            if ($items[$i] != $item) {
                unset($items[$i]);
            }
        }
        $this->setItems($items);
    }

    /**
     * {@inheritdoc}
     */
    public function addItem($item)
    {
        $items = $this->getItems();
        $items[] = $item;
        $this->setItems($items);
    }

    /**
     * {@inheritdoc}
     */
    public function destroy()
    {
        $this->sessionService->destroySession();
    }
}
