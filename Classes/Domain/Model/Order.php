<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Class Order.
 */
class Order extends AbstractEntity
{
    /**
     * The customerNumber.
     *
     * @var string
     * @validate NotEmpty
     */
    protected $customerNumber = '';

    /**
     * The frontend user.
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $frontendUser = null;

    /**
     * The sections.
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoOrder\Domain\Model\Section>
     * @cascade remove
     */
    protected $sections = null;

    /**
     * The creation DateTime.
     *
     * @var \DateTime
     */
    protected $creationDateTime = null;

    /**
     * The hidden flag of the order.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * The status of the order.
     *
     * @var string
     */
    protected $status = 'new';

    /**
     * The orders total vat.
     *
     * @var float
     */
    protected $totalVat = 0.0;

    /**
     * The orders total with vat.
     *
     * @var float
     */
    protected $totalWithVat = 0.0;

    /**
     * The orders total without vat.
     *
     * @var float
     */
    protected $totalWithoutVat = 0.0;

    /**
     * The orders total discount.
     *
     * @var float
     */
    protected $totalDiscount = 0.0;

    /**
     * __construct.
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties.
     */
    protected function initStorageObjects()
    {
        $this->sections = new ObjectStorage();
    }

    /**
     * Sets the customerNumber.
     *
     * @param string $customerNumber
     */
    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = $customerNumber;
    }

    /**
     * Gets the customerNumber.
     *
     * @return string
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * Sets the frontendUser.
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $frontendUser
     */
    public function setFrontendUser(FrontendUser $frontendUser)
    {
        $this->frontendUser = $frontendUser;
    }

    /**
     * Gets the frontendUser.
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    public function getFrontendUser()
    {
        return $this->frontendUser;
    }

    /**
     * Sets the sections.
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoOrder\Domain\Model\Section> $sections
     */
    public function setSections(ObjectStorage $sections)
    {
        $this->sections = $sections;
    }

    /**
     * Gets the sections.
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoOrder\Domain\Model\Section>
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Sets the creationDateTime.
     *
     * @param \Datetime $creationDateTime
     */
    public function setCreationDateTime(\Datetime $creationDateTime)
    {
        $this->creationDateTime = $creationDateTime;
    }

    /**
     * Gets the creationDateTime.
     *
     * @return \Datetime
     */
    public function getCreationDateTime()
    {
        return $this->creationDateTime;
    }

    /**
     * Sets the status.
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Gets the status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the hidden flag.
     *
     * @param bool $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Gets the hidden flag.
     *
     * @return bool
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Gets the totalVat.
     *
     * @return float
     */
    public function getTotalVat()
    {
        return $this->totalVat;
    }

    /**
     * Set the totalVat.
     *
     * @param float $totalVat
     */
    public function setTotalVat($totalVat)
    {
        $this->totalVat = $totalVat;
    }

    /**
     * Gets the TotalWithVat.
     *
     * @return float
     */
    public function getTotalWithVat()
    {
        return $this->totalWithVat;
    }

    /**
     * Set the TotalWithVat.
     *
     * @param float $totalWithVat
     */
    public function setTotalWithVat($totalWithVat)
    {
        $this->totalWithVat = $totalWithVat;
    }

    /**
     * Gets the totalWithoutVat.
     *
     * @return float
     */
    public function getTotalWithoutVat()
    {
        return $this->totalWithoutVat;
    }

    /**
     * Set the totalWithoutVat.
     *
     * @param float $totalWithoutVat
     */
    public function setTotalWithoutVat($totalWithoutVat)
    {
        $this->totalWithoutVat = $totalWithoutVat;
    }

    /**
     * Gets the totalDiscount.
     *
     * @return float
     */
    public function getTotalDiscount()
    {
        return $this->totalDiscount;
    }

    /**
     * Set the totalDiscount.
     *
     * @param float $totalDiscount
     */
    public function setTotalDiscount($totalDiscount)
    {
        $this->totalDiscount = $totalDiscount;
    }

    /**
     * Get transferable order data.
     *
     * @return array
     */
    public function getTransferableOrderData()
    {
        return [
            'uid' => $this->getUid(),
            'status' => $this->getStatus(),
            'customerNumber' => $this->getCustomerNumber(),
            'frontendUser' => $this->getFrontendUser() instanceof FrontendUser ?
                $this->getFrontendUser()->getUid() : null,
            'timestamp' => $this->getCreationDateTime() instanceof \DateTime ?
                $this->getCreationDateTime()->getTimestamp() : null,
            'sections' => $this->getTransferableSections(),
        ];
    }

    /**
     * Get transferable sections.
     *
     * return array
     */
    protected  function getTransferableSections()
    {
        $sections = [];
        /** @var $section Section */
        foreach ($this->getSections() as $section) {
            $positions = [];
            /** @var $position Position */
            foreach ($section->getPositions() as $position) {
                $positions[] = [
                    'title' => $position->getTitle(),
                    'uid' => $position->getUid(),
                    'articlenumber' => $position->getArticleNumber(),
                    'quantity' => $position->getQuantity(),
                    'price' => $position->getPrice(),
                ];
            }
            $sections[] = [
                'title' => $section->getTitle(),
                'positions' => $positions,
            ];
        }

        return $sections;
    }
}
