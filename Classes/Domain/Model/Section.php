<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Class Section
 * @package Riconet\RicoOrder\Domain\Model
 */
class Section extends AbstractEntity
{

    /**
     * The title.
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * The positions.
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoOrder\Domain\Model\Position>
     * @cascade remove
     */
    protected $positions = null;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->positions = new ObjectStorage();
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gets the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the positions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoOrder\Domain\Model\Position> $positions
     * @return void
     */
    public function setPositions(ObjectStorage $positions)
    {
        $this->positions = $positions;
    }

    /**
     * Gets the price
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Riconet\RicoOrder\Domain\Model\Position>
     */
    public function getPositions()
    {
        return $this->positions;
    }

}
