<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Domain\Model;

/**
 * Class CartItem.
 */
class CartItem
{
    /**
     * title.
     *
     * @var string
     */
    protected $title;

    /**
     * articleNumber.
     *
     * @var string
     */
    protected $articleNumber;

    /**
     * record table.
     *
     * @var string
     */
    protected $recordTable;

    /**
     * record uid.
     *
     * @var int
     */
    protected $recordUid;

    /**
     * quantity.
     *
     * @var int
     */
    protected $quantity;

    /**
     * price.
     *
     * @var float
     */
    protected $price = 0.0;

    /**
     * class.
     *
     * @var string
     */
    protected $class;

    /**
     * @var string
     */
    protected $section;

    /**
     * CartItem constructor.
     *
     * @param string $title
     * @param string $recordTable
     * @param int    $recordUid
     * @param int    $quantity
     * @param string $class
     * @param string $section
     */
    public function __construct($title = '', $recordTable = '', $recordUid = 0, $quantity = 0, $class = '', $section = '')
    {
        $this->title = $title;
        $this->recordTable = $recordTable;
        $this->recordUid = $recordUid;
        $this->quantity = $quantity;
        $this->class = $class;
        $this->section = $section;
    }

    /**
     * Get the title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title.
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the articleNumber.
     *
     * @return string $articleNumber
     */
    public function getArticleNumber()
    {
        return $this->articleNumber;
    }

    /**
     * Sets the articleNumber.
     *
     * @param string $articleNumber
     */
    public function setArticleNumber($articleNumber)
    {
        $this->articleNumber = $articleNumber;
    }

    /**
     * Get the recordTable.
     *
     * @return string
     */
    public function getRecordTable()
    {
        return $this->recordTable;
    }

    /**
     * Sets the recordTable.
     *
     * @param string $recordTable
     */
    public function setRecordTable($recordTable)
    {
        $this->recordTable = $recordTable;
    }

    /**
     * Get the recordUid.
     *
     * @return int
     */
    public function getRecordUid()
    {
        return $this->recordUid;
    }

    /**
     * Sets the recordUid.
     *
     * @param int $recordUid
     */
    public function setRecordUid($recordUid)
    {
        $this->recordUid = $recordUid;
    }

    /**
     * Get the quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set the quantity.
     *
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get the price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get the price.
     *
     * @@param double $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get the class.
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Sets the class.
     *
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * Get the section.
     *
     * @return string
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Sets the section.
     *
     * @param string $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }
}
