<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Position.
 */
class Position extends AbstractEntity
{
    /**
     * The title.
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * articleNumber.
     *
     * @var string
     * @validate NotEmpty
     */
    protected $articleNumber = '';

    /**
     * The quantity.
     *
     * @var int
     */
    protected $quantity = 0;

    /**
     * The price.
     *
     * @var float
     */
    protected $price = 0.0;

    /**
     * The unit price.
     *
     * @var float
     */
    protected $unitPrice = 0.0;

    /**
     * The record table.
     *
     * @var string
     */
    protected $recordTable = '';

    /**
     * The record uid.
     *
     * @var int
     */
    protected $recordUid = 0;

    /**
     * Sets the title.
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gets the title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the articleNumber.
     *
     * @return string $articleNumber
     */
    public function getArticleNumber()
    {
        return $this->articleNumber;
    }

    /**
     * Sets the articleNumber.
     *
     * @param string $articleNumber
     */
    public function setArticleNumber($articleNumber)
    {
        $this->articleNumber = $articleNumber;
    }

    /**
     * Sets the quantity.
     *
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Gets the quantity.
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * Sets the price.
     *
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Gets the price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the recordTable.
     *
     * @param string $recordTable
     */
    public function setRecordTable($recordTable)
    {
        $this->recordTable = $recordTable;
    }

    /**
     * Gets the recordTable.
     *
     * @return string
     */
    public function getRecordTable()
    {
        return $this->recordTable;
    }

    /**
     * Sets the recordUid.
     *
     * @param int $recordUid
     */
    public function setRecordUid($recordUid)
    {
        $this->recordUid = $recordUid;
    }

    /**
     * Gets the recordUid.
     *
     * @return int
     */
    public function getRecordUid()
    {
        return $this->recordUid;
    }
}
