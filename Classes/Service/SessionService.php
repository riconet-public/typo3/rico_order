<?php
/*
 * This file is part of the "rico_order" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2019 Wolf Utz <utz@riconet.de>, riconet
 */

namespace Riconet\RicoOrder\Service;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class SessionUtility.
 */
class SessionService implements SingletonInterface
{
    /**
     * Session data.
     *
     * @var array
     */
    protected $sessionData = [];

    /**
     * Prefix for the session.
     *
     * @var string
     */
    protected $sessionPrefix = '';

    /**
     * @var TypoScriptFrontendController
     */
    protected $frontendController;

    /**
     * Class Constructor.
     */
    public function __construct()
    {
        $this->frontendController = $this->getTypoScriptFrontendController();
    }

    /**
     * Initialize the session.
     *
     * @param string $sessionPrefix
     */
    public function initSession($sessionPrefix = '')
    {
        $this->setSessionPrefix($sessionPrefix);
        if ($this->frontendController->loginUser) {
            $this->sessionData = $this->frontendController->fe_user->getKey('user', $this->sessionPrefix);
        } else {
            $this->sessionData = $this->frontendController->fe_user->getKey('ses', $this->sessionPrefix);
        }
        $this->sessionData = $this->frontendController->fe_user->getKey('ses', $this->sessionPrefix);
    }

    /**
     * Stores current session.
     */
    protected function storeSession()
    {
        if ($this->frontendController->loginUser) {
            $this->frontendController->fe_user->setKey('user', $this->sessionPrefix, $this->getSessionData());
        } else {
            $this->frontendController->fe_user->setKey('ses', $this->sessionPrefix, $this->getSessionData());
        }
        $this->frontendController->fe_user->setKey('ses', $this->sessionPrefix, $this->getSessionData());
        $this->frontendController->storeSessionData();
    }

    /**
     * Destroy the session data for the form.
     */
    public function destroySession()
    {
        if ($this->frontendController->loginUser) {
            $this->frontendController->fe_user->setKey('user', $this->sessionPrefix, null);
        } else {
            $this->frontendController->fe_user->setKey('ses', $this->sessionPrefix, null);
        }
        $this->frontendController->fe_user->setKey('ses', $this->sessionPrefix, null);
        $this->frontendController->storeSessionData();
    }

    /**
     * Set the session Data by $key.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function setSessionData($key, $value)
    {
        $this->sessionData[$key] = $value;
        $this->storeSession();
    }

    /**
     * Retrieve a member of the $sessionData variable.
     * If no $key is passed, returns the entire $sessionData array.
     *
     * @param string $key     parameter to search for
     * @param mixed  $default default value to use if key not found
     *
     * @return mixed returns NULL if key does not exist
     */
    public function getSessionData($key = null, $default = null)
    {
        if (null === $key) {
            return $this->sessionData;
        }

        return isset($this->sessionData[$key]) ? $this->sessionData[$key] : $default;
    }

    /**
     * Set the s prefix.
     *
     * @param string $sessionPrefix
     */
    public function setSessionPrefix($sessionPrefix)
    {
        $this->sessionPrefix = $sessionPrefix;
    }

    /**
     * Checks if a given session key exist.
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasSessionKey($key)
    {
        return isset($this->sessionData[$key]);
    }

    /**
     * @return mixed|TypoScriptFrontendController
     */
    protected function getTypoScriptFrontendController()
    {
        return $GLOBALS['TSFE'];
    }
}
