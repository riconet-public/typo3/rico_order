<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Wolf Utz <utz@riconet.de>, riconet
 *      Created on: 11.06.18 10:13
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Controller;

use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class AbstractUserController.
 */
abstract class AbstractUserController extends ActionController
{
    /**
     * the FrontendUserRepository instance.
     *
     * @var null|FrontendUserRepository
     */
    protected $frontendUserRepository;

    /**
     * injects the FrontendUserRepository.
     *
     * @param FrontendUserRepository $frontendUserRepository the FrontendUserRepository instance
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    /**
     * user.
     *
     * @var FrontendUser|null
     */
    protected $user;

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        $this->user = $this->getFrontendUser();
    }

    /**
     * Sets the frontend user if logged in.
     *
     * @return null|object
     */
    private function getFrontendUser()
    {
        if (isset($GLOBALS['TSFE']->fe_user->user['uid']) && (int) $GLOBALS['TSFE']->fe_user->user['uid'] > 0) {
            $uid = (int) $GLOBALS['TSFE']->fe_user->user['uid'];

            return $this->frontendUserRepository->findByUid($uid);
        }

        return null;
    }
}
