<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Controller;

use Riconet\RicoOrder\Domain\Model\CartItem;
use Riconet\RicoOrder\Utility\CartUtility;
use Riconet\RicoOrder\Utility\ControllerUtility;
use Riconet\RicoOrder\Utility\FlashMessageUtility;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotException;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotReturnException;
use TYPO3\CMS\Install\FolderStructure\Exception\InvalidArgumentException;

/**
 * Class CartController.
 */
class CartController extends AbstractCartController
{
    /**
     * action index.
     */
    public function indexAction()
    {
        $this->view->assignMultiple([
            'items' => $this->cart->getItems(),
        ]);
    }

    /**
     * Action addItem.
     *
     * @param int    $domainObjectUid
     * @param string $domainObjectClass
     * @param int    $quantity
     * @param int    $returnPid
     * @param string $titleField
     * @param string $section
     *
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     * @throws \Exception
     */
    public function addItemAction(
        $domainObjectUid,
        $domainObjectClass,
        $quantity,
        $returnPid = null,
        $titleField = 'title',
        $section = 'Default'
    ) {
        $titleField = empty($titleField) ? 'title' : $titleField;
        $section = empty($section) ? 'Default' : $section;
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $domainObject = $persistenceManager->getObjectByIdentifier($domainObjectUid, $domainObjectClass);
        $cartItem = CartUtility::convertDomainObjectToCartItem(
            $domainObject,
            $quantity > 0 ? $quantity : 1,
            $titleField,
            $section
        );
        $cartItem->setPrice(CartUtility::getMappedProperty($this->typoScript, $cartItem, 'price'));
        $cartItem->setArticleNumber(CartUtility::getMappedProperty($this->typoScript, $cartItem, 'article_number'));
        // Add Signal for manipulation the cart item before it gets added to the cart.
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            'addItemBeforeItemGetsAdded',
            [$domainObject, &$cartItem]
        );
        $this->cart->setItems(CartUtility::addItem($this->cart->getItems(), $cartItem));
        $this->addFlashMessage(
            FlashMessageUtility::addItem($domainObject->_getProperty($titleField)),
            '',
            AbstractMessage::OK
        );
        $uri = ControllerUtility::redirectToPid(
            $this->getControllerContext(),
            is_null($returnPid) ? $GLOBALS['TSFE']->id : $returnPid
        );
        $this->cacheService->clearPageCache(is_null($returnPid) ? $GLOBALS['TSFE']->id : $returnPid);
        $this->redirectToUri($uri);
    }

    /**
     * Action addItem.
     *
     * @param int    $domainObjectUid
     * @param string $domainObjectClass
     * @param int    $quantity
     * @param int    $cartPid
     * @param string $titleField
     * @param string $section
     *
     * @throws \Exception
     */
    public function addItemAjaxAction(
        $domainObjectUid,
        $domainObjectClass,
        $quantity,
        $cartPid,
        $titleField = 'title',
        $section = 'Default'
    ) {
        $titleField = empty($titleField) ? 'title' : $titleField;
        $section = empty($section) ? 'Default' : $section;
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $domainObject = $persistenceManager->getObjectByIdentifier($domainObjectUid, $domainObjectClass);
        $cartItem = CartUtility::convertDomainObjectToCartItem(
            $domainObject,
            $quantity > 0 ? $quantity : 1,
            $titleField,
            $section
        );
        $cartItem->setPrice(CartUtility::getMappedProperty($this->typoScript, $cartItem, 'price'));
        $cartItem->setArticleNumber(CartUtility::getMappedProperty($this->typoScript, $cartItem, 'article_number'));
        // Add Signal for manipulation the cart item before it gets added to the cart.
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            'addItemBeforeItemGetsAdded',
            [$domainObject, &$cartItem]
        );
        $this->cart->setItems(CartUtility::addItem($this->cart->getItems(), $cartItem));
        $uri = ControllerUtility::redirectToPid($this->getControllerContext(), $cartPid);
        $this->view->assignMultiple([
            'item' => $cartItem,
            'linkToCart' => $uri,
        ]);
    }

    /***
     * action removeItem
     *
     * @return void
     * @throws InvalidArgumentException
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     */
    public function removeItemAction()
    {
        if (!$this->request->hasArgument('index')) {
            throw new InvalidArgumentException('Argument index not found!');
        }
        $index = (int) $this->request->getArgument('index');
        $itemsBefore = $this->cart->getItems();
        /** @var $itemToRemove CartItem */
        $itemToRemove = isset($itemsBefore[$index]) && $itemsBefore[$index] instanceof CartItem ?
            $itemsBefore[$index] : null;
        $title = $itemToRemove instanceof CartItem ? $itemToRemove->getTitle() : '';
        $items = CartUtility::removeItem($itemsBefore, $itemToRemove);
        $this->cart->setItems($items);
        $this->addFlashMessage(FlashMessageUtility::removeItem($title), '', AbstractMessage::OK);
        $this->cacheService->clearPageCache($GLOBALS['TSFE']->id);
        $this->redirect('index');
    }

    /**
     * action edit.
     *
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     * @throws InvalidSlotException
     * @throws InvalidSlotReturnException
     */
    public function editAction()
    {
        $arguments = $this->request->getArguments();
        $items = $this->cart->getItems();
        foreach ($arguments['quantity'] as $index => $quantity) {
            if (isset($items[$index]) && $items[$index] instanceof CartItem) {
                /** @var $item CartItem */
                $item = &$items[$index];
                $this->signalSlotDispatcher->dispatch(
                    __CLASS__,
                    'updateCartItem',
                    [&$item, $index, $arguments, $items]
                );
                $item->setQuantity($quantity > 0 ? $quantity : 1); // We do not want zeros!
            }
        }
        $this->cart->setItems($items);
        $this->addFlashMessage(FlashMessageUtility::updateItems(), '', AbstractMessage::OK);
        $this->cacheService->clearPageCache($GLOBALS['TSFE']->id);
        $this->redirect('index');
    }

    /**
     * action clear.
     *
     * @throws InvalidArgumentException
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     */
    public function clearAction()
    {
        $this->cart->destroy();
        $this->redirect('index');
    }
}
