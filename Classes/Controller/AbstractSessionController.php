<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Controller;

use Riconet\RicoOrder\Service\SessionService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class AbstractSessionController
 * @package Riconet\RicoOrder\Controller
 */
abstract class AbstractSessionController extends ActionController
{

    /**
     * @var string SESSION_PREFIX
     */
    const SESSION_PREFIX = 'rico_order';

    /**
     * The sessionService.
     *
     * @var \Riconet\RicoOrder\Service\SessionService
     */
    protected $sessionService = null;

    /**
     * frontendUserRepository
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository = null;

    /**
     * user
     *
     * @var FrontendUser
     */
    protected $user = null;

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        $this->sessionService = GeneralUtility::makeInstance(SessionService::class);
        $this->sessionService->initSession(self::SESSION_PREFIX);
        $this->user = $this->getFrontendUser();
    }

    /**
     * Sets the frontend user if logged in.
     *
     * @return null|object
     */
    private function getFrontendUser()
    {
        if (isset($GLOBALS['TSFE']->fe_user->user['uid']) && (int) $GLOBALS['TSFE']->fe_user->user['uid'] > 0) {
            $uid = (int) $GLOBALS['TSFE']->fe_user->user['uid'];
            return $this->frontendUserRepository->findByUid($uid);
        }
        return null;
    }

}