<?php
/*
 * This file is part of the "rico_order" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2019 Wolf Utz <utz@riconet.de>, riconet
 */

namespace Riconet\RicoOrder\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use Riconet\RicoOrder\Cart\CartInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class AbstractCartController.
 */
abstract class AbstractCartController extends AbstractUserController
{
    /**
     * @var CartInterface|null
     */
    protected $cart;

    /**
     * The typo script of the extension.
     *
     * @var array
     */
    protected $typoScript = [];

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $this->typoScript = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        );
        $this->cart = self::getCart();
    }

    /**
     * @return CartInterface
     */
    public static function getCart()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var CartInterface $cart */
        $cart = $objectManager->get(CartInterface::class);
        if (!$cart instanceof CartInterface) {
            throw new \RuntimeException(
                'The given class does not implement the interface "\Riconet\RicoOrder\Cart\CartInterface"!',
                1528702923
            );
        }
        $cart->initialize();

        return $cart;
    }
}
