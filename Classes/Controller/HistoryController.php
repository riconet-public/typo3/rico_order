<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoOrder\Controller;

use Riconet\RicoOrder\Domain\Repository\OrderRepository;

/**
 * Class HistoryController.
 */
class HistoryController extends AbstractUserController
{
    /**
     * the OrderRepository instance.
     *
     * @var null|OrderRepository
     */
    protected $orderRepository;

    /**
     * injects the OrderRepository.
     *
     * @param OrderRepository $orderRepository the OrderRepository instance
     */
    public function injectOrderRepository(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * action index.
     */
    public function indexAction()
    {
        $orders = $this->orderRepository->findByFrontendUser($this->user);
        $this->view->assignMultiple([
            'user' => $this->user,
            'orders' => $orders,
        ]);
    }
}
