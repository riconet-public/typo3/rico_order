<?php

return [
    'ctrl' => [
        'title' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,article_number,quantity,unit_price,price,record_table,record_uid,',
        'iconfile' => 'EXT:rico_order/Resources/Public/Icons/Position.svg',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, title, article_number, quantity, unit_price, price, record_table, record_uid',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden;;1, title, article_number, quantity, unit_price, price, record_table, record_uid, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
            ],
        ],
        'article_number' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position.article_number',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
            ],
        ],
        'quantity' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position.quantity',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,int',
            ],
        ],
        'unit_price' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position.unit_price',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,double2',
            ],
        ],
        'price' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position.price',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,double2',
            ],
        ],
        'record_table' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position.record_table',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
            ],
        ],
        'record_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_position.record_uid',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required,int',
            ],
        ],
    ],
];
