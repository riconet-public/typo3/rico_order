<?php

return [
    'ctrl' => [
        'title' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order',
        'label' => 'customer_number',
        'label_userFunc' => 'Riconet\\RicoOrder\\UserFunc\\TCATitleUserFunc->formatCustomerNumberAndCrdateLabel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'customer_number,frontend_user,sections,status,total_vat,total_with_vat,total_without_vat,total_discount,crdate,',
        'iconfile' => 'EXT:rico_order/Resources/Public/Icons/Order.svg',
        'default_sortby' => 'ORDER BY crdate DESC',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, customer_number, frontend_user, sections, status, total_vat, total_with_vat, total_without_vat, total_discount, crdate',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden;;1, customer_number, frontend_user, sections, status, total_vat, total_with_vat, total_without_vat, total_discount, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime, crdate'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'customer_number' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.customer_number',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
            ],
        ],
        'frontend_user' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.frontend_user',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'fe_users',
                'minitems' => 0,
                'maxitems' => true,
            ],
        ],
        'sections' => [
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.sections',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_ricoorder_domain_model_section',
                'foreign_field' => 'section_order',
                'maxitems' => 999,
                'appearance' => [
                    'collapseAll' => true,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showAllLocalizationLink' => true,
                ],
            ],
        ],
        'status' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.status',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'total_vat' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.total_vat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,double2',
            ],
        ],
        'total_with_vat' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.total_with_vat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,double2',
            ],
        ],
        'total_without_vat' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.total_without_vat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,double2',
            ],
        ],
        'total_discount' => [
            'exclude' => true,
            'label' => 'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_domain_model_order.total_discount',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,double2',
            ],
        ],
        'crdate' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'crdate',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
                'readOnly' => true,
            ],
        ],
    ],
];
