<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$_EXTKEY = 'rico_order';

/**
 * Register plugin cart.
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Riconet.'.$_EXTKEY,
    'cart',
    'LLL:EXT:rico_order/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_cart'
);

/**
 * Register plugin checkout.
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Riconet.'.$_EXTKEY,
    'checkout',
    'LLL:EXT:'.$_EXTKEY.'/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_checkout'
);

/**
 * Register plugin checkout.
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Riconet.'.$_EXTKEY,
    'history',
    'LLL:EXT:'.$_EXTKEY.'/Resources/Private/Language/locallang_db.xlf:tx_ricoorder_history'
);

/**
 * Add flexform for plugin cart.
 */
$pluginSignature = str_replace('_', '', $_EXTKEY).'_cart';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:'.$_EXTKEY.'/Configuration/FlexForm/Cart.xml'
);

/**
 * Add flexform for plugin cart.
 */
$pluginSignature = str_replace('_', '', $_EXTKEY).'_checkout';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:'.$_EXTKEY.'/Configuration/FlexForm/Checkout.xml'
);
