# Record Cart

### Dependencies
TYPO3 `8.7.0-8.7.99`

*Extensions*
* typoscript_rendering `2.0.0-2.0.99`
* vhs `3.0.0-5.1.0`

## Overview
This extension provides a cart to store almost any type of records, a checkout system and an order generation. This extension is designed to be very extendable.

## Installation
- Activate the extension via the Extensions module.
- Include the static typo script file.

## Information
To change the used vat value, change the extension configurations found under the extension manager.

### Using a different Cart storage resource (Adapter)
To use a  different storage, you have to create an cart adapter class, by implementing the interface `Riconet\RicoOrder\Cart\CartInterface`.

After that, you have to register the class to the object manager, using the following typo script:
````typo3_typoscript
config.tx_extbase {
    objects {
        Riconet\RicoOrder\Cart\CartInterface {
            className = YOUR\CUSTOM\CART\IMPLEMENTATION\CLASS
        }
    }
}
````